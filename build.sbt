organization := "tv.cntt"
name         := "eon_backend"
version      := "1.0-SNAPSHOT"

scalaVersion := "2.12.1"
scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked")

// Xitrum requires Java 8
javacOptions ++= Seq("-source", "1.8", "-target", "1.8")


//------------------------------------------------------------------------------

libraryDependencies += "tv.cntt" %% "xitrum" % "3.28.4"

libraryDependencies += "com.typesafe" % "config" % "1.3.1"

// Xitrum uses SLF4J, an implementation of SLF4J is needed
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.2"

// For writing condition in logback.xml
libraryDependencies += "org.codehaus.janino" % "janino" % "3.0.7"

libraryDependencies += "org.webjars.bower" % "bootstrap-css" % "3.3.6"

// Scalate template engine config for Xitrum -----------------------------------
libraryDependencies += "tv.cntt" %% "xitrum-scalate" % "2.8.0"

// https://mvnrepository.com/artifact/postgresql/postgresql
libraryDependencies += "postgresql" % "postgresql" % "9.1-901.jdbc4"

libraryDependencies += "com.github.fommil" %% "spray-json-shapeless" % "1.4.0"

libraryDependencies += "org.squeryl" %% "squeryl" % "0.9.5-7"

libraryDependencies += "c3p0" % "c3p0" % "0.9.1.2"

// https://mvnrepository.com/artifact/com.mashape.unirest/unirest-java
libraryDependencies += "com.mashape.unirest" % "unirest-java" % "1.4.9"

// https://mvnrepository.com/artifact/com.github.albfernandez/javadbf
//libraryDependencies += "com.github.albfernandez" % "javadbf" % "1.4.0"

// https://mvnrepository.com/artifact/org.jamel.dbf/dbf-reader
libraryDependencies += "org.jamel.dbf" % "dbf-reader" % "0.0.3"


libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"


// https://mvnrepository.com/artifact/com.googlecode.juniversalchardet/juniversalchardet
libraryDependencies += "com.googlecode.juniversalchardet" % "juniversalchardet" % "1.0.3"

// https://mvnrepository.com/artifact/org.apache.tika/tika-core
libraryDependencies += "org.apache.tika" % "tika-core" % "1.14"


// Precompile Scalate templates
scalateSettings
ScalateKeys.scalateTemplateConfig in Compile := Seq(TemplateConfig(
  baseDirectory.value / "src" / "main" / "scalate",
  Seq(),
  Seq(Binding("helper", "xitrum.Action", importMembers = true))
))

// xgettext i18n translation key string extractor is a compiler plugin ---------

autoCompilerPlugins := true
addCompilerPlugin("tv.cntt" %% "xgettext" % "1.5.1")
scalacOptions += "-P:xgettext:xitrum.I18n"

// Put config directory in classpath for easier development --------------------

// For "sbt console"
unmanagedClasspath in Compile += Attributed.blank(baseDirectory.value / "config")

// For "sbt run"
unmanagedClasspath in Runtime += Attributed.blank(baseDirectory.value / "config")

// Copy these to target/xitrum when sbt xitrum-package is run
XitrumPackage.copy("config", "public", "script")


fork in run := true