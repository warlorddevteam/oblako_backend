package app.tokenizer.controller.api

import xitrum.{Action , SkipCsrfCheck}
import xitrum.annotation.{Swagger}



trait DefaultLayout extends Action with SkipCsrfCheck {
  override def layout = renderViewNoLayout[DefaultLayout]()
}
