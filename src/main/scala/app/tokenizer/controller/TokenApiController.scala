package app.tokenizer.controller

import xitrum.{Action}
import scala.collection.mutable.ArrayBuffer
import xitrum.annotation.{GET, CacheActionMinute}

@GET("/test")
@CacheActionMinute(1)
class TestsIndex extends Action {
  def execute() {
    var array = ArrayBuffer(1,2,3,4,5,6,7,8).filter((i) => i % 2 == 0)
    respondJson(array)
 } 
}



