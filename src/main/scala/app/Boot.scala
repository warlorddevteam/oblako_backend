package app

import xitrum.Server

object Boot {
  def main(args: Array[String]) {
    val db = new Db
    db.configureDb()
    db.userSchema()
    db.alkoSchema()
    Server.start()
  }
}


