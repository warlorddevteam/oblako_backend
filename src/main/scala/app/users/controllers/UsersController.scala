package app.users.controller

import app.Db
import xitrum.{FutureAction, SkipCsrfCheck}
import xitrum.annotation.{CacheActionMinute, GET, Swagger}
import app.users.entity.UserEntity
import app.users.models.UserModel
import org.squeryl.PrimitiveTypeMode._

@GET("/user/test")
@CacheActionMinute(1)
class SendUser extends FutureAction with SkipCsrfCheck {
  def execute() {
    val a = UserModel.getTestUser
    respondJson(a)
  }
}

@GET("/user/save")
@Swagger(
  Swagger.Tags("save test user"),
  Swagger.Description("simple save"),
  Swagger.Response(200, "Ok"),
  Swagger.Response(304, "Db not conf")
)
@CacheActionMinute(1)
class SaveUser extends FutureAction with SkipCsrfCheck {
  def execute() {
    UserModel.saveTestUser()
    respondText("Ok")
  }
}

@GET("/user/all")
@Swagger(
  Swagger.Tags("get all users"),
  Swagger.Description("get all users"),
  Swagger.Response(200, "json"),
  Swagger.Response(404, "Db does not have records")
)
@CacheActionMinute(1)
class AllUsers extends FutureAction with SkipCsrfCheck {
  def execute() {
    val all = UserModel.allUsers()
    respondJson(all)
  }
}



