package app.users.entity


import org.squeryl.KeyedEntity
/**
  * Created by renegatumsoul on 10.07.17.
  */
class UserEntity(
                  val id: Long = 0,
                  var login: String,
                  var password : String,
                  var firstName: String,
                  var lastName: Option[String],
                  var middleName: Option[String],
                  var role : Option[Int],
                  var department: Option[Int],
                  var active: Boolean = false
                ) extends  KeyedEntity[Long] {
  def this() = this(0,"","","", Some(""), Some(""),Some(0), Some(0),false)
}

