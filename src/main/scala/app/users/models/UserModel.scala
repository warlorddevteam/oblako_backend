package app.users.models

import app.Db
import app.users.entity.UserEntity
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.{Query, Schema}

import scala.collection.immutable
import scala.collection.mutable.ListBuffer


object UserModel extends Db {

  def saveTestUser(): Unit = {
    inTransaction {
      users.insert(new UserEntity(0, "test", "test", "Stas", Some("Test"), Some("Test"), Some(2), Some(3), true))
    }
  }

  def getTestUser: UserEntity = {
    inTransaction(users.where(a => a.id === 1).single)
  }

  def allUsers(): ListBuffer[UserEntity] = {
    transaction {
      var list: ListBuffer[UserEntity] = new ListBuffer[UserEntity]
      val query = transaction(from(users)(select(_)))
      query.foreach(list += _)
      return list
    }
  }
}
