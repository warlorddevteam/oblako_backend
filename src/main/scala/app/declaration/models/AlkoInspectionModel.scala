package app.declaration.models

import app.Db
import app.declaration.entity.AlkoInspektionDictionaryEntity
import org.squeryl.PrimitiveTypeMode._
import scala.collection.mutable.{ArrayBuffer, ListBuffer}


object AlkoInspectionModel extends Db{

  def saveAllDictionary(map: ListBuffer[AnyRef]): Unit = {
    inTransaction {
          var i = 0
          while (i < map.length ) {
            alkoInspektionDictionaryEntity.insert(
              new AlkoInspektionDictionaryEntity(
                0,Some(map(i)), Some(map(i+1)), Some(map(i+2)), Some(map(i+3)),Some(map(i+4)), Some(map(i+5)),Some(map(i+6))
                ,Some(map(i+7)),Some(map(i+8)),Some(map(i+9)),Some(map(i+10)),Some(map(i+11))))

            {
              i += 13
            }

            println(s"ok now $i")
            println("map lenght : " + map.length)
          }
    }
  }

  def saveEntity(entity: AlkoInspektionDictionaryEntity): Unit = {
    inTransaction {
        alkoInspektionDictionaryEntity.insert(entity)
        println(s"ok entity complet")
      }
    }

  def getAll(): ListBuffer[AlkoInspektionDictionaryEntity] = {
    transaction {
      var list: ListBuffer[AlkoInspektionDictionaryEntity] = new ListBuffer[AlkoInspektionDictionaryEntity]
            val query = transaction(from(alkoInspektionDictionaryEntity)(select(_)))
            query.foreach(list += _)
            return list
    }
  }
}
