package app.declaration.services

import java.io.{File, FileInputStream, IOException, InputStream}
import java.nio.charset.Charset

import org.jamel.dbf.processor.{DbfProcessor, DbfRowMapper}
import org.jamel.dbf.utils.DbfUtils
import app.declaration.entity.AlkoInspektionDictionaryEntity
import app.declaration.models.AlkoInspectionModel
import java.util

import org.jamel.dbf.processor.DbfRowProcessor

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import org.jamel.dbf.processor.DbfProcessor

import java.io.BufferedInputStream
import java.io.FileInputStream
import java.nio.charset.Charset
import java.nio.charset.CharsetDecoder

object GosAlcogolService {

  def getGosAlcogolHdbk(path: String = "tmp/test.dbf"): Unit = {
    Charset.forName("cp866")
    var mut: ListBuffer[AnyRef] = ListBuffer[AnyRef]()
    val dbf: File = new File(path)
    DbfProcessor.processDbf(dbf, InsertDataDbf)
  }

  def getGosAlcogolText(path: String = "tmp/test.dbf"): Unit = {
    DbfProcessor.writeToTxtFile(
      new File(path),
      new File("altnames.txt"),
      Charset.forName("cp866"))
  }


  private object InsertDataDbf extends DbfRowProcessor {
    private var totalSum = .0
    private var rowsCount = 0

    override def processRow(row: Array[AnyRef]): Unit = { // assuming that there are prices in the 4th column
       def convert(i: Integer): String = {
         new String(row(i).asInstanceOf[Array[Byte]],Charset.forName("cp866"))
     }
      AlkoInspectionModel.saveEntity(
        new AlkoInspektionDictionaryEntity(
          0,
          Some(convert(0)),
          Some(convert(1)),
          Some(convert(2)),
          Some(convert(3)),
          Some(row(4).toString), Some(row(5).toString),
          Some(row(6).toString), Some(row(7).toString),
          Some(row(8).toString), Some(row(9).toString),
          Some(row(10).toString), Some(row(11).toString)
        )
      )
        rowsCount += 13
    }



    private def getTotalSum = totalSum
    private def getRowsCount = rowsCount


  }

}