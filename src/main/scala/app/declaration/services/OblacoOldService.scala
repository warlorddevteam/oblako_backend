package app.declaration.services

import com.mashape.unirest.http.{HttpResponse, JsonNode, Unirest}
import org.json



class OblacoOldService {
  def getTestDeclaration: json.JSONArray = {
    val getTest = Unirest.get("https://tategais.ru/json.php").asJson().getBody.getArray
    getTest
  }
}