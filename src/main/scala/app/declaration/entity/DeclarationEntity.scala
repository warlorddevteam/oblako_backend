package app.declaration.entity


import org.squeryl.KeyedEntity
/**
  * Created by renegatumsoul on 10.07.17.
  */
class DeclarationEntity(
                  val id: Long = 0,
                  var Quantity: Double = 0,
                  var AlcCode : Long = 0,
                  var FSRAR: Long = 0,
                  var RestsShopMini_id: Long = 0 ) extends  KeyedEntity[Long] {
  def this() = this(0,0.0,0,0)
}
