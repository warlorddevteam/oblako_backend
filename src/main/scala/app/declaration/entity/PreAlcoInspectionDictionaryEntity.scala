package app.declaration.entity

class PreAlcoInspectionDictionaryEntity() {
  val SERTIF: Option[AnyRef] = Some("")
  val NAME: Option[AnyRef] = Some("")
  val LKVP: Option[AnyRef] = Some("")
  val GOST: Option[AnyRef] = Some("")
  val COMP_NID: Option[AnyRef] = Some("")
  val DICT_ID: Option[AnyRef] = Some("")
  val CAPACITY: Option[AnyRef] = Some("")
  val DATE_ROZ: Option[AnyRef] = Some("")
  val UKP: Option[AnyRef] = Some("")
  val UKP1: Option[AnyRef] = Some("")
  val GRAD_S: Option[AnyRef] = Some("")
  val PROD_ID: Option[AnyRef] = Some("")
}
