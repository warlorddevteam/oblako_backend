package app.declaration.entity

import org.squeryl.KeyedEntity

class AlkoInspektionDictionaryEntity(
  val id : Long = 0,
  val SERTIF: Option[AnyRef],
  val NAME: Option[AnyRef],
  val LKVP: Option[AnyRef],
  val GOST: Option[AnyRef],
  val COMP_NID: Option[AnyRef],
  val DICT_ID: Option[AnyRef],
  val CAPACITY: Option[AnyRef],
  val DATE_ROZ: Option[AnyRef],
  val UKP: Option[AnyRef],
  val UKP1: Option[AnyRef],
  val GRAD_S: Option[AnyRef],
  val PROD_ID: Option[AnyRef] ) extends KeyedEntity[Long] {
  def this() = this(0, Some(""), Some(""), Some(""), Some(""), Some(""), Some(""), Some(""), Some(""), Some(""), Some(""), Some(""), Some(""))

}



