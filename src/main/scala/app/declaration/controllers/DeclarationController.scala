package app.declaration.controllers

import java.io.File

import app.declaration.models.AlkoInspectionModel
import app.declaration.services.{GosAlcogolService, OblacoOldService}
import app.users.models.UserModel
import xitrum.{Action, FutureAction, SkipCsrfCheck}
import xitrum.annotation.{CacheActionMinute, GET, POST, Swagger}
import io.netty.handler.codec.http.multipart.FileUpload

@GET("/alco/all")
@CacheActionMinute(1)
class SendTestDeclartion extends FutureAction with SkipCsrfCheck {
  def execute() {
    val a = AlkoInspectionModel.getAll()
    respondJson(a)
  }
}

@POST("/alco/upload")
@CacheActionMinute(1)
class FileEGaisUpload extends FutureAction with SkipCsrfCheck {
  def execute() {
    paramo[FileUpload]("file") match {
      case Some(file) =>
        print("Uploaded " + file.getFilename)
        try {
          GosAlcogolService.getGosAlcogolHdbk(file.getFile.getAbsolutePath)
        } finally {
          respondJson("ok")
        }
      case None =>
        respondJson("Please upload a nonempty file")
    }
  }
}

@POST("/alco/upload/test")
@CacheActionMinute(1)
class FileTestGaisUpload extends FutureAction with SkipCsrfCheck {
  def execute() {
    paramo[FileUpload]("file") match {
      case Some(file) =>
        print("Uploaded " + file.getFilename)
        try {
          GosAlcogolService.getGosAlcogolText(file.getFile.getAbsolutePath)
        } finally {
          respondJson("ok")
        }
      case None =>
        respondJson("Please upload a nonempty file")
    }
  }
}
