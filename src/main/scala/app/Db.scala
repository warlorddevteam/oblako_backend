package app

import app.declaration.entity.AlkoInspektionDictionaryEntity
import app.users.entity.UserEntity
import org.squeryl.PrimitiveTypeMode.transaction
import org.squeryl.{Schema, Table}
/**
  * Created by renegatumsoul on 10.07.17.
  */

class Db extends Schema with DbTrait {

  val users = table[UserEntity]("users")
  val alkoInspektionDictionaryEntity = table[AlkoInspektionDictionaryEntity]("alkoinspection_dictionary")

  def userSchema(): Unit = {
    transaction {
      try {
        this.users.schema.drop
        logger.info("Drop")
      } catch {
        case e: Exception => logger.info("Fail")
      } finally {
        this.users.schema.create
        logger.info("Created")
      }
    }
  }

  def alkoSchema(): Unit = {
    transaction {
      try {
        this.alkoInspektionDictionaryEntity.schema.drop
        logger.info("Drop")
      } catch {
        case e: Exception => logger.info("Fail")
      } finally {
        this.alkoInspektionDictionaryEntity.schema.create
        logger.info("Created")
      }
    }
  }
}
